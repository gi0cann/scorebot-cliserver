import socket, ssl

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
ssl_sock = ssl.wrap_socket(s,
                           cert_reqs=ssl.CERT_NONE,
                           ssl_version=ssl.PROTOCOL_TLSv1)
ssl_sock.connect(('127.0.0.1',6667))
ssl_sock.send(b'hello ~MySSL !')
print(ssl_sock.recv(4096))
ssl_sock.close()